class RouteName {
  static const String splashScreen = '/';
  static const String mainAppScreen = '/mainApp';
  static const String homeScreen = '/homeScreen';
  static const String loginScreen = '/loginScreen';
  static const String changeProfileScreen = '/changeProfileScreen';
  static const String forgotPasswordScreen = '/forgotPasswordScreen';
}