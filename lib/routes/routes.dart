import 'package:food_delivery_app/routes/route_name.dart';
import 'package:food_delivery_app/views/home_screen/home_screen.dart';
import 'package:food_delivery_app/views/login/forgot_password.dart';
import 'package:food_delivery_app/views/login/login_screen.dart';
import 'package:food_delivery_app/views/main_app.dart';
import 'package:food_delivery_app/views/profile_detail/change_profile.dart';
import 'package:food_delivery_app/views/splash/splash_screen.dart';
import 'package:get/get.dart';
class AppRoutes {
  static appRoutes() => [
    GetPage(
      name: RouteName.splashScreen,
      page: () => const SplashScreen(),
      transitionDuration: const Duration(milliseconds: 200),
      transition: Transition.leftToRightWithFade,
    ),
    GetPage(
      name: RouteName.mainAppScreen,
      page: () => MainApp(),
      transitionDuration: const Duration(milliseconds: 200),
      transition: Transition.leftToRightWithFade,
    ),
    GetPage(
      name: RouteName.loginScreen,
      page: () => MainApp(),
      transitionDuration: const Duration(milliseconds: 200),
      transition: Transition.leftToRightWithFade,
    ),
    GetPage(
      name: RouteName.loginScreen,
      page: () => LoginScreen(),
      transitionDuration: const Duration(milliseconds: 200),
      transition: Transition.leftToRightWithFade,
    ),
    GetPage(
      name: RouteName.forgotPasswordScreen,
      page: () => ForgotPassword(),
      transitionDuration: const Duration(milliseconds: 200),
      transition: Transition.leftToRightWithFade,
    ),
    GetPage(
      name: RouteName.changeProfileScreen,
      page: () => ChangeProfile(),
      transitionDuration: const Duration(milliseconds: 200),
      transition: Transition.leftToRightWithFade,
    ),
    GetPage(
      name: RouteName.forgotPasswordScreen,
      page: () => HomeScreen(),
      transitionDuration: const Duration(milliseconds: 200),
      transition: Transition.leftToRightWithFade,
    ),
  ];
}