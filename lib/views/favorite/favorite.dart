import 'package:flutter/material.dart';
import 'package:food_delivery_app/views/main_screen/main_screen_controller.dart';
import 'package:get/get.dart';

class Favorite extends StatelessWidget {
  Favorite({super.key});
  final mainScreenController = Get.put(MainScreenController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.blueGrey,
        child: Center(
          child: ElevatedButton(
            onPressed: mainScreenController.toggleDrawer,
            child: Text("Toggle Drawer"),
          ),
        ),
      ),
    );
  }
}
