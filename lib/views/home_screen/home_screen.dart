import 'package:flutter/material.dart';
import 'package:food_delivery_app/utils/color_utils.dart';
import 'package:food_delivery_app/views/home_screen/home_controller.dart';
import 'package:food_delivery_app/views/main_screen/main_screen_controller.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});
  final homeController = Get.put(HomeController());
  final mainScreenController = Get.put(MainScreenController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.COLOR_BG_SCREEN_1,
      body: Column(
        children: [
          const SizedBox(height: 70,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 45),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ElevatedButton(
                  onPressed: mainScreenController.toggleDrawer,
                  child: Text("Toggle Drawer"),
                ),
                Image.asset('assets/icons/shopping-cart-logo.png')
              ],
            ),
          ),
          const SizedBox(height: 40,),
          const Padding(
              padding: EdgeInsets.only(left: 45),
              child: Row(
                children: [
                  Text('Delicious \nfood for you',
                    style: TextStyle(
                        fontSize: 34,
                        fontWeight: FontWeight.w700,
                        color: Colors.black),
                  ),
                ],
              )
          ),
          const SizedBox(height: 40,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 45),
            child: Container(
              decoration: BoxDecoration(
                  color: ColorUtils.COLOR_BG_SCREEN_SEARCH,
                  borderRadius: BorderRadius.circular(30)
              ),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                      borderSide: BorderSide.none
                  ),
                  hintText: 'Search',
                  hintStyle: TextStyle(color: Colors.black.withOpacity(0.5), fontSize: 17, fontWeight: FontWeight.w600),
                  prefixIcon: const Icon(Icons.search, size: 25, color: Colors.black,),
                ),
              ),
            ),
          ),
          const SizedBox(height: 30,),
          Padding(
            padding: const EdgeInsets.only(left: 45),
            child: TabBar(
                isScrollable: true,
                dividerColor: Colors.transparent,
                indicatorColor: ColorUtils.COLOR_TEXT,
                indicatorSize: TabBarIndicatorSize.label,
                labelColor: ColorUtils.COLOR_TEXT,
                labelStyle: const TextStyle(fontSize: 18,),
                unselectedLabelColor: Colors.grey,
                controller: homeController.tabHomeController,
                onTap: (value) =>  homeController.setTabIndex(value),
                tabs: const [
                  Tab(
                    child: Text('Foods',),
                  ),
                  Tab(
                    child: Text('Drinks',),
                  ),
                  Tab(
                    child: Text('Snacks',),
                  ),
                  Tab(
                    child: Text('Sauce',),
                  ),
                ]
            ),
          )
        ],
      ),
    );
  }
}
