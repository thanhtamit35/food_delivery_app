import 'package:flutter/material.dart';
import 'package:food_delivery_app/views/main_screen/main_screen_controller.dart';
import 'package:get/get.dart';
class HomeController extends GetxController with GetSingleTickerProviderStateMixin{
  MainScreenController mainScreenController = Get.put(MainScreenController());

  TabController? tabHomeController;
  int indexHomeTab = 0;

  @override
  void onInit() {
    super.onInit();
    tabHomeController = TabController(length: 4, vsync: this);
  }


  @override
  void dispose() {
    super.dispose();
    tabHomeController!.dispose();
  }

  void setTabIndex(int value){
    indexHomeTab = value;
  }
}