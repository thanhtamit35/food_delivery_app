import 'package:flutter/material.dart';
import 'package:food_delivery_app/utils/color_utils.dart';
import 'package:food_delivery_app/views/login/login_controller.dart';
import 'package:food_delivery_app/widget/button_custom.dart';
import 'package:get/get.dart';

class SignUpView extends StatelessWidget {
  SignUpView({super.key});
  final LoginController loginController = Get.put(LoginController());


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorUtils.COLOR_BG_SCREEN_1,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 50),
        child: Column(
          children: [
            const SizedBox(height: 30,),
            TextFormField(
              controller: loginController.enteremailController,
              cursorColor: Colors.black,
              style: const TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.w600),
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 20),
                labelText: 'Enter Email Address',
                labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),

              ),
            ),
            const SizedBox(height: 20,),
            TextFormField(
              controller: loginController.enterpasswordController,
              cursorColor: Colors.black,
              obscureText: true,
              obscuringCharacter: '*',
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 20),
                labelText: 'Enter Password',
                labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),
              ),
            ),
            const SizedBox(height: 20,),
            TextFormField(
              controller: loginController.enterthepasswordController,
              cursorColor: Colors.black,
              obscureText: true,
              obscuringCharacter: '*',
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.symmetric(vertical: 20),
                labelText: 'Re-Enter Password',
                labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),
              ),
            ),
            const SizedBox(height: 20,),
            Row(
              children: [
                Text('You have an account?  ', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600, color: Colors.black.withOpacity(0.3)),),
                InkWell(
                  onTap: () => loginController.tabController?.animateTo(0),
                    child: const Text('Login Here', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w800, color: ColorUtils.COLOR_TEXT),)
                )
              ],
            ),
            const SizedBox(height: 80,),
            const ButtonCustom(
              textContent: 'Sign Up',
              color: ColorUtils.COLOR_TEXT,
              height: 70,
              width: 314,
              borderRadius: 30,
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600, color: Colors.white),
            )

          ],
        ),
      ),
    );
  }
}
