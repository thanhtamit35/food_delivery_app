import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController with GetSingleTickerProviderStateMixin  {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  TextEditingController enteremailController = TextEditingController();
  TextEditingController enterpasswordController = TextEditingController();
  TextEditingController enterthepasswordController = TextEditingController();

  TextEditingController newemailController = TextEditingController();
  TextEditingController newpasswordController = TextEditingController();
  TextEditingController repasswordController = TextEditingController();


  bool isShowPassword = false;

  TabController? tabController;
  int indexTab = 0;

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    tabController!.dispose();
  }

  void setTabIndex(int value){
    indexTab = value;
  }

}