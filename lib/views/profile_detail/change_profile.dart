import 'package:flutter/material.dart';
import 'package:food_delivery_app/utils/color_utils.dart';
import 'package:food_delivery_app/views/profile_detail/profile_detail_controller.dart';
import 'package:food_delivery_app/widget/button_custom.dart';
import 'package:get/get.dart';

class ChangeProfile extends StatelessWidget {
  ChangeProfile({super.key});

  final ProfileDetailController profileDetailController = Get.put(ProfileDetailController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            children: [
              const SizedBox(height: 50,),
              Row(
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                      child: const Icon(Icons.arrow_back_ios)
                  ),
                  const SizedBox(width: 60,),
                  const Text('Change profile', style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22),),
                ],
              ),
              const SizedBox(height: 20,),
              const CircleAvatar(
                 backgroundColor: Colors.red,
                radius: 50,
              ),
              const SizedBox(height: 15,),
              const Text('This is my fucken NAME', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600),),
              Text('This is my fucken EMAIL', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600, color: Colors.black.withOpacity(0.5)),),
              const SizedBox(height: 30,),
              TextFormField(
                controller: profileDetailController.firstNameController,
                // cursorColor: Colors.black,
                style: const TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.w600),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(vertical: 20),
                  hintText: 'First name',
                  labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),

                ),
              ),
              const SizedBox(height: 30,),
              TextFormField(
                controller: profileDetailController.lastNameController,
                cursorColor: Colors.black,
                style: const TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.w600),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(vertical: 20),
                  hintText: 'Last name',
                  labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),

                ),
              ),
              const SizedBox(height: 30,),
              TextFormField(
                controller: profileDetailController.phoneController,
                cursorColor: Colors.black,
                keyboardType: TextInputType.phone,
                style: const TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.w600),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(vertical: 20),
                  hintText: 'Phone Number',
                  labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),

                ),
              ),
              const SizedBox(height: 30,),
              TextFormField(
                controller: profileDetailController.mailController,
                cursorColor: Colors.black,
                keyboardType: TextInputType.emailAddress,
                style: const TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.w600),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(vertical: 20),
                  hintText: 'Email',
                  labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),

                ),
              ),
              const SizedBox(height: 30,),
              TextFormField(
                controller: profileDetailController.addressController,
                cursorColor: Colors.black,
                keyboardType: TextInputType.streetAddress,
                style: const TextStyle(color: Colors.black, fontSize: 17, fontWeight: FontWeight.w600),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(vertical: 20),
                  hintText: 'Address',
                  labelStyle: TextStyle(color: Colors.black.withOpacity(0.4), fontSize: 15, fontWeight: FontWeight.w600),

                ),
              ),
              const SizedBox(height: 40,),
              const ButtonCustom(
                textContent: 'Update profile',
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.w600, color: ColorUtils.COLOR_TEXT_2),
                color: ColorUtils.COLOR_TEXT,
                width: 315,
                height: 70,
                borderRadius: 30,
              )
            ],
          ),
        ),
      ),
    );
  }
}
