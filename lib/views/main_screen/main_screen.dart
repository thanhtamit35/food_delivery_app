import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:food_delivery_app/utils/color_utils.dart';
import 'package:food_delivery_app/views/home_screen/home_screen.dart';
import 'package:food_delivery_app/views/main_screen/main_screen_controller.dart';
import 'package:get/get.dart';

class MainScreen extends StatelessWidget {
  MainScreen({super.key});

  final mainScreenController = Get.put(MainScreenController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainScreenController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          leading: Container(),
          title: const Center(child: Text('Bạn bè chơi golf', ),),
          actions: [
            IconButton(onPressed: (){}, icon: const Icon(Icons.notifications),),
            IconButton(onPressed: (){}, icon: const Icon(Icons.account_circle_rounded),),
          ],
        ),
        body: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: mainScreenController.pageController,
          children: [
            HomeScreen(),
            Container(),
            Container(
              color: Colors.blue,
              child: const Center(
                child: Text('Page 3'),
              ),
            ),
            Container(),
            Container(
              color: Colors.blue,
              child: const Center(
                child: Text('Page 5'),
              ),
            ),
          ],
        ),
        bottomNavigationBar: SizedBox(
          height: 80,
          child: BottomNavigationBar(
            currentIndex: mainScreenController.selectedIndex,
            onTap: (value) {
              mainScreenController.setSelectedIndex(value);
              mainScreenController.jumToPage(value);
            },
            items: const [
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  color: ColorUtils.COLOR_ICON_BOTTOM,
                  size: 32,
                ),
                activeIcon: Icon(
                  Icons.home,
                  color: ColorUtils.COLOR_TEXT,
                  size: 32,
                ),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.favorite_border,
                  color: ColorUtils.COLOR_ICON_BOTTOM,
                  size: 32,
                ),
                activeIcon: Icon(
                  Icons.favorite_border,
                  color: ColorUtils.COLOR_TEXT,
                  size: 32,
                ),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.person_outline,
                  color: ColorUtils.COLOR_ICON_BOTTOM,
                  size: 32,
                ),
                activeIcon: Icon(
                  Icons.person_outline,
                  color: ColorUtils.COLOR_TEXT,
                  size: 32,
                ),
                label: '',
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.history,
                  color: ColorUtils.COLOR_ICON_BOTTOM,
                  size: 32,
                ),
                activeIcon: Icon(
                  Icons.history,
                  color: ColorUtils.COLOR_TEXT,
                  size: 32,
                ),
                label: '',
              ),
            ],
          ),
        ),
      );
    },);
  }
}
